#!/usr/bin/env python3

# Copyright (c) 2018-2019 ISciences, LLC.
# All rights reserved.
#
# WSIM is licensed under the Apache License, Version 2.0 (the "License").
# You may not use this file except in compliance with the License. You may
# obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function  # Avoid bombing in Python 2 before we even hit our version check

import sys
if sys.version_info.major < 3:
    print("Must use Python 3")
    sys.exit(1)

import argparse
import glob
import json
import psycopg2
import os
import re

from typing import Dict, Union, NamedTuple

from geoserver_rest_configure import geoserver_rest as geoserver
from geoserver_rest_configure import geoserver_http as geoserver_http
from geoserver_rest_configure import mosaics as mosaics

WSIM_WORKSPACES_DIR = '/opt/wsim_data'  # currently baked into the GeoServer container

SqlParameter = NamedTuple('SqlParameter', [('name', str), ('default', str), ('regex', str)])


def parse_args(args):
    parser = argparse.ArgumentParser('Create and configure GeoServer workspaces for WSIM data')

    parser.add_argument('--user',
                        help='GeoServer administrator user',
                        default='admin')

    parser.add_argument('--password',
                        help='GeoServer administrator password',
                        default='geoserver')

    parser.add_argument('--url',
                        help='GeoServer REST API URL (e.g., http://localhost/geoserver/rest)',
                        default='http://localhost:8080/geoserver/rest'
                        )

    parser.add_argument('--dbhost',
                        help='Postgres database host')

    parser.add_argument('--dbport',
                        type=int,
                        help='Postgres database port',
                        default=5432)

    parser.add_argument('--dbname',
                        help='Postgres database name')

    parser.add_argument('--dbuser',
                        help='Postgres database user',
                        default='geoserver')

    parser.add_argument('--dbpass',
                        help='Postgres database password',
                        default='geoserver')

    parser.add_argument('--continue-on-error',
                        action='store_true',
                        help='Continue configuration after server responds with an error code')

    parser.add_argument('workspace',
                        help='Workspace name')

    parser.add_argument('action',
                        help='Action (init or harvest)')

    parsed = parser.parse_args(args)

    return parsed


def read_file(filename):
    """
    Return the entire contents of a file as a binary
    :param filename:
    """
    with open(filename, 'rb') as f:
        return f.read()


def init_styles(workspace, style_dir):
    for filename in os.listdir(style_dir):
        if not filename.endswith('.sld'):
            continue
        style_name = filename.rsplit('.', maxsplit=1)[0]
        geoserver.add_style(workspace, style_name, read_file(os.path.join(style_dir, filename)))


def init_natural_earth(workspace, db_params):
    geoserver.add_postgis_datastore(workspace, db_params, 'natural_earth')

    with psycopg2.connect(**db_params) as conn:
        cur = conn.cursor()

        cur.execute('SELECT table_name FROM information_schema.tables WHERE table_schema = %s', ('natural_earth', ))
        feature_types = set(row[0] for row in cur.fetchall())

    natural_earth_styles = {
        'lakes'                    : 'water',
        'ocean'                    : 'water',
        'glaciated_areas'          : 'ice',
        'admin_0_countries'        : 'countries',
        'admin_1_states_provinces' : 'provinces',
        'geography_regions_polys'  : 'ne_geography_regions_polys'
    }

    for feature_type in feature_types:
        suffix = re.sub('^ne_\d+m_', '', feature_type)
        style = natural_earth_styles[suffix]

        geoserver.add_feature_type(workspace, 'natural_earth', feature_type, default_style=style)

    for res in (10, 50, 110):
        mask_components = [ft.format(res) for ft in ('ne_{}m_lakes', 'ne_{}m_ocean', 'ne_{}m_glaciated_areas')]
        if all(lyr in feature_types for lyr in mask_components):
            geoserver.add_layer_group(workspace,
                                      'natural_earth_{}m_mask'.format(res),
                                      [{'type': 'layer', 'name' : layer} for layer in mask_components])


def init_wsim_postgis(workspace: str, db_params: Dict[str, Union[str, int]], view_definitions: str):
    store_name = 'wsim'
    geoserver.add_postgis_datastore(workspace, db_params, store_name)

    with open(view_definitions, 'r') as jsonfile:
        for view in json.load(jsonfile):
            params = [SqlParameter(**p) for p in view['parameters']]

            geoserver.add_postgis_view(workspace,
                                       store_name,
                                       view['name'],
                                       view['query'].replace('${WORKSPACE}', workspace),
                                       view['id_field'],
                                       view['srid'],
                                       view['style'],
                                       params)


def init_mosaic(workspace, db_params, mosaic, defer_harvest=False):
    var_names = [layer['original_name'] for layer in mosaic['layers']]
    indexing_directory = os.path.join(WSIM_WORKSPACES_DIR, workspace, mosaic['directory'])
    dimension_names = [d['name'] for d in mosaic['dimensions']]
    db_schema_name = '_'.join((workspace, mosaic['mosaic_name']))

    datastore_properties = mosaics.create_datastore_properties(db_params, db_schema_name)
    aux = mosaics.create_aux(mosaic['dimensions'], var_names)
    wildcard = mosaic['wildcard']

    if defer_harvest:
        # When we create coverages based on this ImageMosaic, a harvest of all granules
        # matching the wildcard will be triggered. This can be slow if we have a lot of
        # matching files.
        #
        # To avoid this, we can (mostly) defer this harvesting by swapping out the wildcard
        # in the indexer with the filename of a single file matching the wildcard. Then,
        # after the coverages have been created, we restore the original wildcard so that a
        # subsequent harvest request will find the expected files.
        matching_files = glob.glob(os.path.join(indexing_directory, mosaic['wildcard']))
        if matching_files:
            print("Deferring harvest of", mosaic['mosaic_name'])
            wildcard = os.path.basename(matching_files[0])
        else:
            # No files match wildcard anyway, can't defer harvest
            print("Can't defer harvest of", mosaic['mosaic_name'], "because no files match", mosaic['wildcard'])
            defer_harvest = False

    indexer = mosaics.create_indexer(mosaic['dimensions'], var_names, wildcard)

    geoserver.create_image_mosaic(workspace=workspace,
                                  name=mosaic['mosaic_name'],
                                  indexer=indexer,
                                  aux=aux,
                                  datastore_properties=datastore_properties)

    geoserver.harvest_granules(workspace=workspace,
                               name=mosaic['mosaic_name'],
                               location=indexing_directory)

    # Create coverages
    for layer in mosaic['layers']:
        geoserver.add_coverage(workspace,
                               mosaic['mosaic_name'],
                               layer['published_name'],
                               layer['original_name'],
                               default_style=layer.get('style', None),
                               dimensions=dimension_names)

    if defer_harvest:
        # Now that our coverages have been created, revert our wildcard to what it should have been
        indexer = mosaics.create_indexer(mosaic['dimensions'], var_names, mosaic['wildcard'])
        geoserver.create_image_mosaic(workspace=workspace,
                                      name=mosaic['mosaic_name'],
                                      indexer=indexer,
                                      aux=aux,
                                      datastore_properties=datastore_properties)


def init(workspace: str, home_dir: str, embedded_db_params: Dict[str, str], wsim_db_params: Dict[str, str]):
    geoserver.create_workspace(workspace)

    init_styles(workspace, os.path.join(home_dir, 'styles'))

    # Add Natural Earth data
    init_natural_earth(workspace, embedded_db_params)

    # Add elec
    init_wsim_postgis(workspace, wsim_db_params, os.path.join(home_dir, 'views.json'))

    # Add DataStores and layers
    with open(os.path.join(home_dir, 'mosaics.json')) as jsonfile:
        data = json.load(jsonfile)
        for mosaic in data['mosaics']:
            init_mosaic(workspace, embedded_db_params, mosaic, defer_harvest=True)

    # Add hotspot layer groups
    for horizon in ('forecast', 'observed'):
        for style in ('adjusted_', ''):
            # Create hotspot layer
            geoserver.add_layer_group(workspace, 'wsim_hotspot_{}{}'.format(style, horizon), (
                { 'type' : 'layer', 'name' : 'wsim_composite_{}surplus_{}'.format(style, horizon) },
                { 'type' : 'layer', 'name' : 'wsim_composite_{}deficit_{}'.format(style, horizon) },
                { 'type' : 'layer', 'name' : 'wsim_composite_{}both_{}'.format(style, horizon) },
            ))


def harvest(workspace, home_dir):
    """
    Harvest granules for all mosaics
    :param workspace:
    :param home_dir:
    :return:
    """
    with open(os.path.join(home_dir, 'mosaics.json')) as jsonfile:
        data = json.load(jsonfile)

        for mosaic in data['mosaics']:
            indexing_directory = os.path.join(WSIM_WORKSPACES_DIR, workspace, mosaic['directory'])

            geoserver.harvest_granules(workspace=workspace,
                                       name=mosaic['mosaic_name'],
                                       location=indexing_directory)


def main(raw_args):
    args = parse_args(raw_args)

    # Database parameters for embedded PostGIS database, relative to
    # GeoServer. Since the PostGIS database is in the same Docker
    # container as GeoServer and does not accept outside connections,
    # these are all defaults.
    embedded_db_params = {
        'host'     : 'localhost',
        'port'     : 5432,
        'database' : 'geoserver',
        'user'     : 'geoserver',
        'password' : 'geoserver'
    }

    wsim_db_params = {
        'host'     : args.dbhost,
        'port'     : args.dbport,
        'database' : args.dbname,
        'user'     : args.dbuser,
        'password' : args.dbpass
    }

    errors = []
    if args.continue_on_error:
        def error_handler(url, err, data):
            errors.append(dict(url=url, err=err))
    else:
        def error_handler(url, err, data):
            print(err, 'from', url, file=sys.stderr)
            print('Last request body', data[:10000], file=sys.stderr)
            sys.exit(1)

    geoserver_http.configure(args.url, args.user, args.password, error_handler)

    config_dir = os.path.dirname(os.path.realpath(__file__))

    if args.action == 'init':
        init(args.workspace, config_dir, embedded_db_params, wsim_db_params)
        harvest(args.workspace, config_dir)
    elif args.action == 'harvest':
        harvest(args.workspace, config_dir)
    elif args.action == 'delete':
        geoserver.delete_workspace(args.workspace)
    else:
        sys.exit('Unknown task')

    if errors:
        print('Errors occurred:', file=sys.stderr)
        for error in errors:
            print(error['err'], 'from', error['url'], file=sys.stderr)
        sys.exit(1)


if __name__ == '__main__':
    main(sys.argv[1:])
