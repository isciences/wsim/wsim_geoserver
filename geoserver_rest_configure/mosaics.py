# Copyright (c) 2018 ISciences, LLC.
# All rights reserved.
#
# WSIM is licensed under the Apache License, Version 2.0 (the "License").
# You may not use this file except in compliance with the License. You may
# obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import xml.etree.ElementTree as ET

BUILTIN_DIMS = [
    dict(name='the_geom',   type='polygon'),
    dict(name='location',   type='text'),
    dict(name='imageindex', type='int')
]


def create_schema_string(dimensions):
    java_types = {
        'date'    : 'java.util.Date',
        'int'     : 'Integer',
        'text'    : 'String',
        'polygon' : 'Polygon'
    }

    return ','.join(dim['name'] + ':' + java_types[dim['type']] for dim in (dimensions + BUILTIN_DIMS))


def create_aux(dimensions, var_names):
    root = ET.Element("Indexer")

    # Create schema
    schemas = ET.SubElement(root, "schemas")
    schema = ET.SubElement(schemas, "schema", name='default')
    ET.SubElement(schema, "attributes").text = create_schema_string(dimensions)

    # Create coverages
    coverages = ET.SubElement(root, "coverages")
    for var in var_names:
        coverage = ET.SubElement(coverages, "coverage")
        ET.SubElement(coverage, "name").text = var
        ET.SubElement(coverage, "schema", ref="default")

    return ET.tostring(root, encoding='UTF-8', method='xml').decode('utf8')


def create_indexer(dimensions, var_names, wildcard):

    collector_names = {
        'date' : 'TimestampFileNameExtractorSPI',
        'int'  : 'IntegerFileNameExtractorSPI'
    }

    dimnames = [d['name'] for d in dimensions]

    root = ET.Element("Indexer")
    domains = ET.SubElement(root, "domains")
    schemas = ET.SubElement(root, "schemas")
    coverages = ET.SubElement(root, "coverages")
    collectors = ET.SubElement(root, "collectors")
    parameters = ET.SubElement(root, "parameters")

    # Create domains
    for dim in dimnames:
        domain = ET.SubElement(domains, 'domain', name=dim)
        attributes = ET.SubElement(domain, 'attributes')
        ET.SubElement(attributes, 'attribute').text = dim

    # Create schema
    schema = ET.SubElement(schemas, 'schema', name="default")
    ET.SubElement(schema, 'attributes').text = create_schema_string(dimensions)

    # Create coverages
    for var in var_names:
        coverage = ET.SubElement(coverages, 'coverage')
        ET.SubElement(coverage, 'name').text = var
        ET.SubElement(coverage, 'schema', ref='default')
        domains = ET.SubElement(coverage, 'domains')

        for dim in dimensions:
            ET.SubElement(domains, 'domain', ref=dim['name'])

    # Create collectors
    for dim in dimensions:
        collector = ET.SubElement(collectors, 'collector', name=dim['name'] + 'Collector')
        ET.SubElement(collector, 'spi').text = collector_names[dim['type']]
        ET.SubElement(collector, 'value').text = dim['regex']
        ET.SubElement(collector, 'mapped').text = dim['name']

    # Set parameters
    ET.SubElement(parameters, 'parameter', name='AbsolutePath', value='true')
    ET.SubElement(parameters, 'parameter', name='AuxiliaryFile', value='aux.xml')
    ET.SubElement(parameters, 'parameter', name='CanBeEmpty', value='true')
    ET.SubElement(parameters, 'parameter', name='Wildcard', value=wildcard)
    # Don't specify an IndexingDirectory here.
    # This causes an automatic harvest of all granules after creation.
    # https://sourceforge.net/p/geoserver/mailman/message/36272390/

    return ET.tostring(root, encoding='UTF-8', method='xml').decode('utf8')


def create_datastore_properties(db_params, schema):
    format_params = { 'schema' : schema }
    format_params.update(db_params)

    return """user={user}
port={port}
passwd={password}
url=jdbc:\:postgresql\:{database}
host={host}
database={database}
driver=org.postgresql.Driver
schema={schema}
Estimated\ extends=false
SPI=org.geotools.data.postgis.PostgisNGDataStoreFactory""".format_map(format_params)
