# Copyright (c) 2018 ISciences, LLC.
# All rights reserved.
#
# WSIM is licensed under the Apache License, Version 2.0 (the "License").
# You may not use this file except in compliance with the License. You may
# obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import io
import xml.etree.ElementTree as ET
import zipfile

from typing import Dict, Optional, List, NamedTuple

from . import geoserver_http

SqlParameter = NamedTuple('SqlParameter',
                          [('name', str),
                           ('default', str),
                           ('regex', str)])


def create_workspace(name: str) -> None:
    """
    Create a workspace with a given name
    :param name:
    """
    workspace = ET.Element('workspace')
    ET.SubElement(workspace, 'name').text = name

    geoserver_http.send_xml('workspaces', workspace)


def delete_workspace(name: str) -> None:
    """
    Delete a workspace with a given name
    :param name:
    """

    geoserver_http.send_content('workspaces/{}/?recurse=true'.format(name), None, None, method='DELETE')


def add_style(workspace, name, sld_contents):
    """
    Create a style in a workspace, given an SLD
    :param workspace: destination workspace
    :param name: name of style
    :param sld_contents: xml body of sld
    """
    filename = '{}.sld'.format(name)

    style = ET.Element('style')
    ET.SubElement(style, 'name').text = name
    ET.SubElement(style, 'filename').text = filename

    geoserver_http.send_xml('workspaces/{}/styles'.format(workspace), style)

    return geoserver_http.send_content('workspaces/{}/styles/{}'.format(workspace, name),
                                       data=sld_contents,
                                       method='PUT',
                                       content_type='application/vnd.ogc.sld+xml')


def set_style(workspace: str, layer_name: str, style: str) -> None:
    """
    Set the default style for a layer
    :param workspace: workspace containing layer
    :param layer_name: name of layer
    :param style: name of style
    """
    layer = ET.Element('layer')
    default_style = ET.SubElement(layer, 'defaultStyle')
    ET.SubElement(default_style, 'name').text = workspace + ':' + style

    geoserver_http.send_xml('layers/{}:{}'.format(workspace, layer_name),
                            layer,
                            method='PUT')


def add_feature_type(workspace, datastore, name, default_style=None):
    """
    Publish a vector feature type from a datastore, optionally setting its default style
    :param workspace: workspace containing datastore
    :param datastore: name of datastore
    :param name: name of feature type to publish
    :param default_style: name of default style
    """
    feature_type = ET.Element('featureType')
    ET.SubElement(feature_type, 'name').text = name

    geoserver_http.send_xml('workspaces/{}/datastores/{}/featuretypes'.format(workspace, datastore),
                            feature_type)

    if default_style:
        set_style(workspace, name, default_style)


def add_layer_group(workspace, name, contents):
    """
    Create a layer group in a workspace
    :param workspace: name of workspace
    :param name: name of layer group
    :param contents: list of items in the layer group. a list of dictionaries with keys "name" and "type", where
                     "type" is either "layer" or "layerGroup"
    """
    layer_group = ET.Element('layerGroup')
    ET.SubElement(layer_group, 'name').text = name
    publishables = ET.SubElement(layer_group, 'publishables')

    for item in contents:
        published = ET.SubElement(publishables, 'published', type=item['type'])
        ET.SubElement(published, 'name').text = workspace + ':' + item['name']

    geoserver_http.send_xml('workspaces/{}/layergroups'.format(workspace),
                            layer_group)


def harvest_granules(workspace, name, location):
    """
    Harvest the granules in an ImageMosaic
    :param workspace: name of workspace
    :param name: name of image mosaic
    :param location: directory to harvest
    """
    geoserver_http.send_content('workspaces/{}/coveragestores/{}/external.imagemosaic'.format(workspace, name),
                                'file://' + location,
                                method='POST',
                                content_type='text/plain')


def create_image_mosaic(*, workspace, name, indexer, aux, datastore_properties):
    """
    Create an ImageMosaic
    :param workspace: name of workspace
    :param name: name of mosaic
    :param indexer: text contents of "indexer.xml" file
    :param aux: text contents of "aux.xml" file
    :param datastore_properties: text contents of "datastore.properties" file
    """
    zipbuf = io.BytesIO()

    with zipfile.ZipFile(zipbuf, mode='w', compression=zipfile.ZIP_DEFLATED) as zf:
        zf.writestr('indexer.xml', indexer)
        zf.writestr('aux.xml', aux)
        zf.writestr('datastore.properties', datastore_properties)

    zipbuf.seek(0)
    geoserver_http.send_content('workspaces/{}/coveragestores/{}/file.imagemosaic?configure=none'.format(workspace, name),  # noqa
                                zipbuf.read(),
                                method='PUT',
                                content_type='application/zip')


def add_coverage(workspace, coveragestore, coverage_name, var_name, default_style=None, dimensions=None):
    """
    Expose a coverage in a coverage store
    :param workspace: name of workspace
    :param coveragestore: name of coverage store (image mosaic)
    :param coverage_name: name of coverage
    :param var_name: netCDF variable name
    :param default_style: default style for coverage
    :param dimensions: list of dimensions e.g., (time, window, target)
    """
    coverage = ET.Element('coverage')
    ET.SubElement(coverage, 'name').text = coverage_name
    ET.SubElement(coverage, 'nativeName').text = var_name

    geoserver_http.send_xml('workspaces/{}/coveragestores/{}/coverages'.format(workspace, coveragestore),
                            coverage)

    if default_style:
        set_style(workspace, coverage_name, default_style)

    if dimensions:
        coverage = ET.Element('coverage')
        ET.SubElement(coverage, 'name').text = coverage_name
        metadata = ET.SubElement(coverage, 'metadata')

        for dim in dimensions:
            if dim != 'time':
                dim = 'custom_dimension_' + dim.upper()

            entry = ET.SubElement(metadata, 'entry', key=dim)
            dimension_info = ET.SubElement(entry, 'dimensionInfo')
            ET.SubElement(dimension_info, 'enabled').text = 'true'
            ET.SubElement(dimension_info, 'presentation').text = 'LIST'

        geoserver_http.send_xml('workspaces/{}/coveragestores/{}/coverages/{}'.format(workspace, coveragestore, coverage_name),  # noqa
                                coverage,
                                method='PUT')


def add_postgis_datastore(workspace: str, db_params: Dict[str,str], schema_name: str) -> None:
    """
    Add a PostGIS datastore
    :param workspace: name of workspace
    :param db_params: connection params for database
    :param schema_name: name of schema/datastore
    """

    store = ET.Element('dataStore')
    ET.SubElement(store, 'name').text = schema_name

    params = ET.SubElement(store, 'connectionParameters')
    ET.SubElement(params, 'entry', { 'key' : 'host' }).text = db_params['host']
    ET.SubElement(params, 'entry', { 'key' : 'port' }).text = str(db_params['port'])
    ET.SubElement(params, 'entry', { 'key' : 'database' }).text = db_params['database']
    ET.SubElement(params, 'entry', { 'key' : 'user' }).text = db_params['user']
    ET.SubElement(params, 'entry', { 'key' : 'passwd' }).text = db_params['password']
    ET.SubElement(params, 'entry', { 'key' : 'dbtype' }).text = 'postgis'

    ET.SubElement(params, 'entry', { 'key' : 'schema' }).text = schema_name

    # Disable on-the-fly geometry simplifcation to preserve alignment between admin0 and
    # admin1 boundaries in projected coordinate systems
    ET.SubElement(params, 'entry', { 'key' : 'Support on the fly geometry simplification' }).text = 'false'

    geoserver_http.send_xml('workspaces/{}/datastores'.format(workspace), store)


def add_postgis_view(workspace: str, store_name: str, layer_name: str, sql: str, key_column: str, srid: int, default_style: Optional[str], params: Optional[List[SqlParameter]]) -> None:
    ft = ET.Element('featureType')
    ET.SubElement(ft, 'name').text = layer_name
    #ET.SubElement(ft, 'nativeName').text = name

    ET.SubElement(ft, 'title').text = layer_name
    ET.SubElement(ft, 'srs').text = 'EPSG:{:d}'.format(srid)
    ET.SubElement(ft, 'projectionPolicy').text = 'FORCE_DECLARED'

    metadata = ET.SubElement(ft, 'metadata')
    ET.SubElement(metadata, 'entry', attrib={'key':'cachingEnabled'}).text='true'

    vtablemeta = ET.SubElement(metadata, 'entry', attrib={'key':'JDBC_VIRTUAL_TABLE'})
    vtable = ET.SubElement(vtablemeta, 'virtualTable')
    ET.SubElement(vtable, 'name').text = layer_name
    ET.SubElement(vtable, 'sql').text = sql
    ET.SubElement(vtable, 'escapeSql').text = 'false'
    ET.SubElement(vtable, 'keyColumn').text = key_column
    geometry = ET.SubElement(vtable, 'geometry')
    ET.SubElement(geometry, 'name').text = 'geom'
    ET.SubElement(geometry, 'type').text = 'MultiPolygon'
    ET.SubElement(geometry, 'srid').text = str(srid)

    for p in params:
        parameter = ET.SubElement(vtable, 'parameter')
        ET.SubElement(parameter, 'name').text = p.name
        ET.SubElement(parameter, 'defaultValue').text = str(p.default)
        ET.SubElement(parameter, 'regexpValidator').text = p.regex

    store = ET.SubElement(ft, 'store', attrib={'class': 'dataStore'})
    ET.SubElement(store, 'name').text = store_name

    boxes = [ET.SubElement(ft, 'nativeBoundingBox'), ET.SubElement(ft, 'latLonBoundingBox')]
    for box in boxes:
        ET.SubElement(box, 'minx').text = '-180.0'
        ET.SubElement(box, 'maxx').text = '180.0'
        ET.SubElement(box, 'miny').text = '-90.0'
        ET.SubElement(box, 'maxy').text = '90.0'

    geoserver_http.send_xml('workspaces/{}/datastores/{}/featuretypes'.format(workspace, store_name), ft)

    if default_style:
        set_style(workspace, layer_name, default_style)

