#!/usr/bin/env bash
set -e

LAYER_TYPE=$1
LAYER_NAME=$2
LAYER_RES=${@:3}

TEMPDIR=$(mktemp -d)
TEMPSQL=$(mktemp --suffix .sql)

for RES in ${LAYER_RES}; do 
  wget -P ${TEMPDIR} http://www.naturalearthdata.com/http//www.naturalearthdata.com/download/${RES}m/${LAYER_TYPE}/ne_${RES}m_${LAYER_NAME}.zip
  unzip ${TEMPDIR}/ne_${RES}m_${LAYER_NAME}.zip -d ${TEMPDIR}
  shp2pgsql -s 4326 -D -I ${TEMPDIR}/ne_${RES}m_${LAYER_NAME}.shp natural_earth.ne_${RES}m_${LAYER_NAME} > ${TEMPSQL}
  psql -1 -v ON_ERROR_STOP=1 -f ${TEMPSQL}
done

rm ${TEMPSQL}
rm -rf ${TEMPDIR}
