<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0"
        xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd"
        xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
        xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <NamedLayer>
    <Name>polygon_loss_risk</Name>
    <UserStyle>
      <Title>Loss Risk</Title>
      <FeatureTypeStyle>
        <Rule>
          <Title>No Data</Title>
          <ogc:Filter><ogc:PropertyIsNull><ogc:PropertyName>loss</ogc:PropertyName></ogc:PropertyIsNull></ogc:Filter>
          <PolygonSymbolizer>
            <Fill><CssParameter name="fill">#cecece</CssParameter></Fill>
            <Stroke><CssParameter name="stroke">#232323</CssParameter><CssParameter name="stroke-width">0.5</CssParameter></Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Title>Normal</Title>
          <ogc:Filter><ogc:PropertyIsLessThan><ogc:PropertyName>loss</ogc:PropertyName><ogc:Literal>0.1</ogc:Literal></ogc:PropertyIsLessThan></ogc:Filter>
          <PolygonSymbolizer>
            <Fill><CssParameter name="fill">#ffffff</CssParameter></Fill>
            <Stroke><CssParameter name="stroke">#232323</CssParameter><CssParameter name="stroke-width">0.5</CssParameter></Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Title>Abnormal</Title>
          <ogc:Filter><ogc:And>
            <ogc:PropertyIsGreaterThanOrEqualTo><ogc:PropertyName>loss</ogc:PropertyName><ogc:Literal>0.1</ogc:Literal></ogc:PropertyIsGreaterThanOrEqualTo> 
            <ogc:PropertyIsLessThan><ogc:PropertyName>loss</ogc:PropertyName><ogc:Literal>0.2</ogc:Literal></ogc:PropertyIsLessThan>
          </ogc:And></ogc:Filter>
          <PolygonSymbolizer>
            <Fill><CssParameter name="fill">#ffffb6</CssParameter></Fill>
            <Stroke><CssParameter name="stroke">#232323</CssParameter><CssParameter name="stroke-width">0.5</CssParameter></Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Title>Significant</Title>
          <ogc:Filter><ogc:And>
            <ogc:PropertyIsGreaterThanOrEqualTo><ogc:PropertyName>loss</ogc:PropertyName><ogc:Literal>0.2</ogc:Literal></ogc:PropertyIsGreaterThanOrEqualTo> 
            <ogc:PropertyIsLessThan><ogc:PropertyName>loss</ogc:PropertyName><ogc:Literal>0.4</ogc:Literal></ogc:PropertyIsLessThan>
          </ogc:And></ogc:Filter>
          <PolygonSymbolizer>
            <Fill><CssParameter name="fill">#fecf62</CssParameter></Fill>
            <Stroke><CssParameter name="stroke">#232323</CssParameter><CssParameter name="stroke-width">0.5</CssParameter></Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Title>Severe</Title>
          <ogc:Filter><ogc:And>
            <ogc:PropertyIsGreaterThanOrEqualTo><ogc:PropertyName>loss</ogc:PropertyName><ogc:Literal>0.4</ogc:Literal></ogc:PropertyIsGreaterThanOrEqualTo> 
            <ogc:PropertyIsLessThan><ogc:PropertyName>loss</ogc:PropertyName><ogc:Literal>0.6</ogc:Literal></ogc:PropertyIsLessThan>
          </ogc:And></ogc:Filter>
          <PolygonSymbolizer>
            <Fill><CssParameter name="fill">#fd9242</CssParameter></Fill>
            <Stroke><CssParameter name="stroke">#232323</CssParameter><CssParameter name="stroke-width">0.5</CssParameter></Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Title>Extreme</Title>
          <ogc:Filter><ogc:And>
            <ogc:PropertyIsGreaterThanOrEqualTo><ogc:PropertyName>loss</ogc:PropertyName><ogc:Literal>0.6</ogc:Literal></ogc:PropertyIsGreaterThanOrEqualTo> 
            <ogc:PropertyIsLessThan><ogc:PropertyName>loss</ogc:PropertyName><ogc:Literal>0.8</ogc:Literal></ogc:PropertyIsLessThan>
          </ogc:And></ogc:Filter>
          <PolygonSymbolizer>
            <Fill><CssParameter name="fill">#f14125</CssParameter></Fill>
            <Stroke><CssParameter name="stroke">#232323</CssParameter><CssParameter name="stroke-width">0.5</CssParameter></Stroke>
          </PolygonSymbolizer>
        </Rule>
        <Rule>
          <Title>Exceptional</Title>
          <ogc:Filter>
            <ogc:PropertyIsGreaterThanOrEqualTo><ogc:PropertyName>loss</ogc:PropertyName><ogc:Literal>0.8</ogc:Literal></ogc:PropertyIsGreaterThanOrEqualTo> 
          </ogc:Filter>
          <PolygonSymbolizer>
            <Fill><CssParameter name="fill">#c1002b</CssParameter></Fill>
          </PolygonSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
