<?xml version="1.0" encoding="UTF-8"?>
<sld:StyledLayerDescriptor
  xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld"
  xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc"
  version="1.0.0">
  <sld:NamedLayer>
    <sld:Name>runoff</sld:Name>
    <sld:UserStyle>
      <sld:Name>runoff</sld:Name>
      <sld:Title>Style for runoff (mm)</sld:Title>
      <sld:FeatureTypeStyle>
        <sld:Name>name</sld:Name>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ColorMap>
              <sld:ColorMapEntry color="#f7fbff" opacity="0" quantity="0"   />
              <sld:ColorMapEntry color="#f7fbff" opacity="1" quantity="0"   />
              <sld:ColorMapEntry color="#deebf7" opacity="1" quantity="${25*env('months')}"  label="25 mm/month"  />
              <sld:ColorMapEntry color="#c6dbef" opacity="1" quantity="${50*env('months')}"  label="50 mm/month"  />
              <sld:ColorMapEntry color="#9ecae1" opacity="1" quantity="${100*env('months')}" label="100 mm/month" />
              <sld:ColorMapEntry color="#6baed6" opacity="1" quantity="${150*env('months')}" />
              <sld:ColorMapEntry color="#4292c6" opacity="1" quantity="${200*env('months')}" label="200 mm/month" />
              <sld:ColorMapEntry color="#2171b5" opacity="1" quantity="${250*env('months')}" />
              <sld:ColorMapEntry color="#08519c" opacity="1" quantity="${300*env('months')}" label="300 mm/month" />
              <sld:ColorMapEntry color="#08306b" opacity="1" quantity="${400*env('months')}" />
            </sld:ColorMap>
            <sld:ContrastEnhancement/>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </sld:NamedLayer>
</sld:StyledLayerDescriptor>
