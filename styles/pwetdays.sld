<?xml version="1.0" encoding="UTF-8"?>
<sld:StyledLayerDescriptor
  xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld"
  xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc"
  version="1.0.0">
  <sld:NamedLayer>
    <sld:Name>pwetdays</sld:Name>
    <sld:UserStyle>
      <sld:Name>pwetdays</sld:Name>
      <sld:Title>Style for wet days (0-1)</sld:Title>
      <sld:FeatureTypeStyle>
        <sld:Name>name</sld:Name>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ColorMap>
              <sld:ColorMapEntry color="#FFFFFF" opacity="1" quantity="0"    label="0"    />
              <sld:ColorMapEntry color="#CDEBC6" opacity="1" quantity="0.25" label="25%"  />
              <sld:ColorMapEntry color="#78CAC5" opacity="1" quantity="0.50" label="50%"  />
              <sld:ColorMapEntry color="#3394C3" opacity="1" quantity="0.75" label="75%"  />
              <sld:ColorMapEntry color="#084485" opacity="1" quantity="1"    label="100%" />
            </sld:ColorMap>
            <sld:ContrastEnhancement/>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </sld:NamedLayer>
</sld:StyledLayerDescriptor>
