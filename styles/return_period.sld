<?xml version="1.0" encoding="UTF-8"?><sld:StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" version="1.0.0">
<sld:NamedLayer>
    <sld:Name>Default Styler</sld:Name>
    <sld:UserStyle>
      <sld:Name>Default Styler</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Name>name</sld:Name>
        <sld:Rule>
          <sld:RasterSymbolizer>
	  <sld:ColorMap type="intervals" >
	      <!-- Take care of NA rendering, where NA = -3.39999999999999996e+38 -->
	      <sld:ColorMapEntry color="#9B0039" quantity="-1001" opacity="0"/>
	      <!-- [-inf, -40): -->
              <sld:ColorMapEntry color="#9B0039" opacity="1" quantity="-40" label=">40"/>
              <sld:ColorMapEntry color="#D44135" opacity="1" quantity="-20" label="20-40"/>
              <sld:ColorMapEntry color="#FF8D43" opacity="1" quantity="-10" label="10-20"/>
              <sld:ColorMapEntry color="#FFC754" opacity="1" quantity="-5" label="5-10"/>
	      <!-- [-5, -3): -->
              <sld:ColorMapEntry color="#FFEDA3" opacity="1" quantity="-3" label="3-5"/>

	      <!-- [-3, -3): -->
              <sld:ColorMapEntry color="#FFFFFF" opacity="0" quantity="-3"/>
	      <!-- [-3, 3): -->
              <sld:ColorMapEntry color="#FFFFFF" opacity="0" quantity="3"/>

              <!-- blues -->
	      <!-- [3, 5): -->
              <sld:ColorMapEntry color="#CEFFAD" opacity="1" quantity="5" label="3-5"/>
              <sld:ColorMapEntry color="#00F3B5" opacity="1" quantity="10" label="5-10"/>
              <sld:ColorMapEntry color="#00CFCE" opacity="1" quantity="20" label="10-20"/>
              <sld:ColorMapEntry color="#009ADE" opacity="1" quantity="40" label="20-40"/>
              <sld:ColorMapEntry color="#0045B5" opacity="1" quantity="1001" label=">40"/>
            </sld:ColorMap>
            <sld:ContrastEnhancement/>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </sld:NamedLayer>
</sld:StyledLayerDescriptor>
