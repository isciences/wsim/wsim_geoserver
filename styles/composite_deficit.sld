<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld
http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" version="1.0.0">
  <NamedLayer>
    <Name>composite_deficit</Name>
    <UserStyle>
      <FeatureTypeStyle>
        <Rule>
          <RasterSymbolizer>
            <Opacity>1.0</Opacity>
            <ColorMap type="intervals">              
              <ColorMapEntry color="#9B0039" quantity="-40"  label="40" opacity="1"/>
              <ColorMapEntry color="#D44135" quantity="-20"  label="20" opacity="1"/>
              <ColorMapEntry color="#FF8D43" quantity="-10"  label="10" opacity="1"/>
              <ColorMapEntry color="#FFC754" quantity="-5"   label="5"  opacity="1"/>
              <ColorMapEntry color="#FFEDA3" quantity="-3"   label="3"  opacity="1"/>
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
