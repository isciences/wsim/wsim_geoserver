<?xml version="1.0" encoding="UTF-8"?>
<sld:StyledLayerDescriptor
  xmlns="http://www.opengis.net/sld" xmlns:sld="http://www.opengis.net/sld"
  xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc"
  version="1.0.0">
  <sld:NamedLayer>
    <sld:Name>soil_moisture</sld:Name>
    <sld:UserStyle>
      <sld:Name>soil_moisture</sld:Name>
      <sld:Title>Style for soil moisture (mm)</sld:Title>
      <sld:FeatureTypeStyle>
        <sld:Name>name</sld:Name>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ColorMap>
              <sld:ColorMapEntry color="#fdae61" opacity="0" quantity="0"/>
              <sld:ColorMapEntry color="#fdae61" opacity="1" quantity="0"/>
              <sld:ColorMapEntry color="#fee08b" opacity="1" quantity="20"  label="20 mm"/>
              <sld:ColorMapEntry color="#ffffbf" opacity="1" quantity="40"  label="40 mm"/>
              <sld:ColorMapEntry color="#e6f598" opacity="1" quantity="60"  label="60 mm"/>
              <sld:ColorMapEntry color="#abdda4" opacity="1" quantity="80"  label="80 mm"/>
              <sld:ColorMapEntry color="#66c2a5" opacity="1" quantity="100" label="100 mm"/>
              <sld:ColorMapEntry color="#3288bd" opacity="1" quantity="150" label="150 mm"/>
              <sld:ColorMapEntry color="#5e4fa2" opacity="1" quantity="200" label="200 mm"/>
            </sld:ColorMap>
            <sld:ContrastEnhancement/>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </sld:NamedLayer>
</sld:StyledLayerDescriptor>
