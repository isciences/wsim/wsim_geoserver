<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld
http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" version="1.0.0">
  <NamedLayer>
    <Name>composite_surplus</Name>
    <UserStyle>
      <Title>A raster style</Title>
      <FeatureTypeStyle>
        <Rule>
          <RasterSymbolizer>
            <Opacity>1.0</Opacity>
            <ColorMap type="intervals">
              <ColorMapEntry color="#FFFFFF" opacity="0" quantity="3" label="3"/>
              <ColorMapEntry color="#FFD6E9" opacity="1" quantity="5" label="5"/>
              <ColorMapEntry color="#FFB3E6" opacity="1" quantity="10" label="10"/>
              <ColorMapEntry color="#FD80D2" opacity="1" quantity="20" label="20"/>
              <ColorMapEntry color="#C13FB2" opacity="1" quantity="40" label="40"/>
              <ColorMapEntry color="#89009B" opacity="1" quantity="1e6" />
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
