<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld
http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" version="1.0.0">
  <NamedLayer>
    <Name>composite_surplus</Name>
    <UserStyle>
      <FeatureTypeStyle>
        <Rule>
          <RasterSymbolizer>
            <Opacity>1.0</Opacity>
            <ColorMap type="intervals">
              <ColorMapEntry color="#FFFFFF" quantity="3"   label="3"  opacity="0"/>
              <ColorMapEntry color="#CEFFAD" quantity="5"   label="5"  opacity="1"/>
              <ColorMapEntry color="#00F3B5" quantity="10"  label="10" opacity="1"/>
              <ColorMapEntry color="#00CFCE" quantity="20"  label="20" opacity="1"/>
              <ColorMapEntry color="#009ADE" quantity="40"  label="40" opacity="1"/>
              <ColorMapEntry color="#0045B5" quantity="1e6"            opacity="1"/>
            </ColorMap>
          </RasterSymbolizer>
        </Rule>
      </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
