# WSIM is licensed under the Apache License, Version 2.0 (the "License").
# You may not use this file except in compliance with the License. You may
# obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM debian:stretch
RUN apt-get update && apt-get install -y \
      # For GeoServer
      libjna-java \
      libnetcdf11 \
      openjdk-8-jre \
      wget \
      unzip \
      # For enabling CORS
      xmlstarlet \
      # For configure script \
      python3 \
      python3-pip \
      # For PostGIS store to hold ImageMosaic catalogs
      # May not be "best practice" (multiple services in a container)
      # but much simpler than using docker compose with a
      # rapidly-changing API, dependency on a separate container,
      # and unclear benefits
      postgresql-9.6-postgis-2.3 \
      # for shp2pgsql:
      postgis \
      # for implicit_schema:
      build-essential \ 
      postgresql-server-dev-9.6


ENV GEOSERVER_VER 2.13.0
ENV GEOSERVER_HOME /geoserver
ENV GEOSERVER_DATA_DIR /opt/geoserver_data

RUN wget -P /tmp http://downloads.sourceforge.net/project/geoserver/GeoServer/$GEOSERVER_VER/geoserver-$GEOSERVER_VER-bin.zip && \
  unzip /tmp/geoserver-$GEOSERVER_VER-bin.zip && \
  mv geoserver-$GEOSERVER_VER $GEOSERVER_HOME && \
  rm /tmp/geoserver-$GEOSERVER_VER-bin.zip


RUN wget -P /tmp https://downloads.sourceforge.net/project/geoserver/GeoServer/$GEOSERVER_VER/extensions/geoserver-$GEOSERVER_VER-netcdf-plugin.zip && \
  unzip -uo /tmp/geoserver-$GEOSERVER_VER-netcdf-plugin.zip -d $GEOSERVER_HOME/webapps/geoserver/WEB-INF/lib && \
	rm /tmp/geoserver-$GEOSERVER_VER-netcdf-plugin.zip

# enable CORS
RUN xmlstarlet ed --inplace -P \
  -s "//web-app" -t elem -n TEMPNODE -v "" \
  -s "//TEMPNODE" -t elem -n "filter-name" -v "cross-origin" \
  -s "//TEMPNODE" -t elem -n "filter-class" -v "org.eclipse.jetty.servlets.CrossOriginFilter" \
  -r "//TEMPNODE" -v "filter" \
  -s "//web-app" -t elem -n TEMPNODE -v "" \
  -s "//TEMPNODE" -t elem -n "filter-mapping" \
  -s "//TEMPNODE" -t elem -n "filter-name" -v "cross-origin" \
  -s "//TEMPNODE" -t elem -n "url-pattern" -v "/*" \
  -r "//TEMPNODE" -v "filter-mapping" \
  $GEOSERVER_HOME/webapps/geoserver/WEB-INF/web.xml

# Put JNA where GeoServer can find it (not sure how to make it look in /usr/share/java)
RUN cp /usr/share/java/*.jar $GEOSERVER_HOME/webapps/geoserver/WEB-INF/lib

# download implicit schema creation (needed so GeoServer can
# create schemas for image mosaic indexes)
RUN wget -P /tmp https://github.com/dbaston/pg_implicit_schema/archive/master.zip && \
  unzip /tmp/master.zip -d /tmp && \
  cd /tmp/pg_implicit_schema-master && \
  make && \
  make install && \
  rm /tmp/master.zip && \
  rm -rf /tmp/pg_implicit_schema-master

# Install python dependencies
COPY requirements.txt views.json /gs-config/
RUN pip3 install -r /gs-config/requirements.txt

USER postgres

RUN /etc/init.d/postgresql start && \
    psql -c "CREATE USER geoserver PASSWORD 'geoserver' " && \
    psql -c "CREATE DATABASE geoserver OWNER geoserver" && \
    psql -c "CREATE EXTENSION postgis" -d geoserver && \
    psql -c "CREATE EXTENSION implicit_schema" -d geoserver && \
    /etc/init.d/postgresql stop

USER root

RUN mkdir $GEOSERVER_DATA_DIR

ENV PGHOST=localhost
ENV PGPORT=5432
ENV PGDATABASE=geoserver
ENV PGUSER=geoserver
ENV PGPASSWORD=geoserver

# bake in natural earth data
WORKDIR /tmp

COPY load_natural_earth.sh /usr/local/bin

ARG NE_RES="10 50 110"

RUN /etc/init.d/postgresql start && \
  psql -c "CREATE SCHEMA natural_earth" && \
  /usr/local/bin/load_natural_earth.sh physical lakes ${NE_RES} && \
  /usr/local/bin/load_natural_earth.sh physical ocean ${NE_RES} && \
  /usr/local/bin/load_natural_earth.sh physical glaciated_areas ${NE_RES} && \
  /usr/local/bin/load_natural_earth.sh physical geography_regions_polys ${NE_RES} && \
  /usr/local/bin/load_natural_earth.sh cultural admin_0_countries ${NE_RES} && \
  /usr/local/bin/load_natural_earth.sh cultural admin_1_states_provinces ${NE_RES} && \
  /etc/init.d/postgresql stop

WORKDIR $GEOSERVER_HOME

RUN mkdir -p $GEOSERVER_DATA_DIR/netcdf_index_files

ENV PATH="/gs-config:${PATH}"

COPY configure_geoserver.py mosaics.json views.json /gs-config/
COPY styles /gs-config/styles
COPY geoserver_rest_configure /gs-config/geoserver_rest_configure

# add custom projections
COPY epsg.properties $GEOSERVER_DATA_DIR/user_projections/epsg.properties

CMD /etc/init.d/postgresql start && \
    java -jar start.jar \
	-Djava.awt.headless=true \
	-DGEOSERVER_DATA_DIR="$GEOSERVER_DATA_DIR" \
	-DNETCDF_DATA_DIR="$GEOSERVER_DATA_DIR/netcdf_index_files"
